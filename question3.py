# importing the pandas module
import pandas as pd

def add_columns():
# reading a csv file into a variable 
  df = pd.read_csv(filepath)

# adding new columns to the csv file
  df["Total"] = df[['maths', 'science','arts']].sum(axis=1) 
  df["Percentage"]= df[["Total"]].div(3).round(1)
  print(df)

# writing the variable data into the csv file
  df.to_csv(filepath)

# reading the file path
filepath=input("enter the file path")

# calling the function to add new columns into the csv file
add_columns()

