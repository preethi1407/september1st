#Python program to sort (ascending and descending) a dictionary by key

import operator

#input dictionary
ex_dict = {1: 52, 3: 64, 4: 73, 2: 85, 0: 96}

#use the sorted method to sort the dictionary by key
asc = dict(sorted(ex_dict.items(), key=operator.itemgetter(0)))
print('Ascending order : ',asc)

#for the descending order by keeping the value of reverse to True  
desc = dict( sorted(ex_dict.items(), key=operator.itemgetter(0),reverse=True))
print('Descending order : ',desc)

