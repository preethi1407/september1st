# Python3 code to Check for balanced parentheses in an data read from the file

open_braces = ["[","{","("]
closing_braces = ["]","}",")"]

# Function to check parentheses
def is_balanced(input):
	stack = []
	for i in input:
		if i in open_braces:
			stack.append(i)
		elif i in closing_braces:
			pos = closing_braces.index(i)
			if ((len(stack) > 0) and
				(open_braces[pos] == stack[len(stack)-1])):
				stack.pop()
			else:
				return "Unbalanced"
	if len(stack) == 0:
		return "Balanced"
	else:
		return "Unbalanced"


filename=input("Enter the file name: ")

text_file = open(filename, "r")
data = text_file.read()
text_file.close()

input = str(data)
print(input,"-", is_balanced(input))
